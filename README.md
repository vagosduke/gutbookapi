# Gutendex Book API

GutBookAPI is an API service that sits on top of Gutendex API, a free book registry (https://gutendex.com/)

GutBookAPI provides:
    - the ability to search for books in the same manner as you would on https://gutendex.com
    - a stripped-down layer of Guntendex information
    - the functionality to post and review anonymous book reviews
    - some extra goodies

# Setup

## Pre-requisites

These are the pre-requisites to setup a fully-functional development environment
and be able to run the service locally.

- Python 3.9 and pip (native or virtual environment)
- Docker
- docker-compose
- make (optional)

Every external service (DB, cache) is defined in docker-compose, so no need to install these programs on your local machine


## Development environment
It is recommended to setup the development environment using a virtualenv with **pyenv**

```shell
pyenv install 3.9
pyenv virtualenv 3.9 gutbookenv
```

if using pyenv in Linux/Mac, create a `.python-version` file in project root with the virtualenv 
name. This will enable the shell to automatically use that virtual environment when inside that dir.

```
gutbookenv
```

Once virtualenvironment is set-up and enabled, install ALL requirements by running

```shell
pip install -r requirements-dev.txt
```

## Quickstart
This is a quickstart to set-up a local development environment.

Once you have the virtualenv and dev requirements installed do:

1. Create `.env` file (default settings are `DEBUG=True` and `DATABASE_ENGINE=sqlite3`), and configure envars as desired.
    ```shell
    cp .env.template .env
    ```
   
2. Create the sqlite3 database and migrate the schema
    ```shell
    python manage.py migrate
    ```
   
3. (Optional) Load dev fixtures to populate database with some demo data
    ```shell
    make load-dev-fixtures
    ```
   
4. Run devserver
    ```shell
    python manage.py runserver 0.0.0.0:8000
    ```

5. Now you can send any requests to the dev API on http://localhost:8000. E.g. search a term:
    ```shell
    curl http://localhost:8000/api/books/searchs?term=hello \
    -H 'Content-Type: application/json'
    ```

    or (for posting a review):

    ```shell
    curl -X POST http://localhost:8000/api/books/rating \
    -H 'Content-Type: application/json' \
    -d '{"bookId":"100","rating":6,"review":"Best book ever!"}'
    ```


## Database setup
This Project uses **Postgres** database by default. For dev purposes, **SQLite3** is also supported

### Postgres
By default, the project uses Postgres. Postgres can be either installed on the local machine, 
or started from docker-compose.

To start the docker-compose do:

```shell
docker-compose up database
```

or 

```shell
make start-db
```

After that database can be accessed with any program on `localhost:5432`.

To setup connections with the dev server or any gutbookapi server, set the env variables in OS or the `.env` file

**Note:** To make database container persistent, uncomment the following line in docker-compose 
`postgres-data:/var/lib/postgresql/data`.


### SQLite3
For local development purposes, SQLite3 is also supported. 
Simply set the env variable `DATABASE_ENGINE=sqlite3` (see below for details). 
Running migrations will also create the database file. 


### Migrate Database schema
Once the database service is running, create the scheme using the migration files

```shell
python manage.py migrate
```
**Note** that in the case of sqlite3, this will also create the database file

**Note** that environment variables must be set accordingly beforehand


### Dev fixtures
Once the database is created and migrated, dev fixtures are provided in order to populate the
application with some demo data. For this, run the makefile command:

```shell
make load-dev-fixtures
```

**Note** that re-loading the fixtures, will overwrite all changes.

## Cache Setup
The application can use Redis cache to store locally information that retrieves externally 
in order to improve performance and reduce external calls.

Cache is disabled by default. Enable it in the `.env` file (see below). To start a local Redis
service, simply run

```shell
make start-cache
```

## Django Setup

### Environment
This project uses `dotenv` to read environment variables from the `.env` file. 

Copy this file from the template

```shell
cp .env.template .env
```

### Build Python image
If running dockerized, Build the Docker image. This will create a production-like image with 
the Django wsgi service served using a `gunicorn` server

```shell
docker-compose build gutbookapi
```

or directly with Docker

```shell
docker build -t gutbookapi .
```

### Run Dockerized
Make sure to have already created the database and run the migrations before this.

To run the production-like dockerized service using docker-compose service (will also build): 

```shell
docker-compose up --build gutbookapi
```

Service will be available on http://localhost:8000/

**Note:** by default, the above will try to use the Postgres database defined in the 
`docker-compose.yml`. To use a local sqlite3 file-based database instead, set the envvar in the
`docker-compose.yml` file to sqlite3

```yaml
    environment:
      DATABASE_ENGINE: sqlite3
```

### Run native devserver
(Strongly recommended for development and debugging)

Make sure to have already created the database and run the migrations before this.

To run the service using a local devserver  use:

```shell
python manage.py runserver 0.0.0.0:8000
```

**Note:** by default, the above will try to use a Postgres database. To use a local sqlite3
file-based database, after creating the `.env` file, set the `DATABASE_ENGINE=sqlite3`.
Then, create db and run migrations using `python manage.py migrate`

Service will be available on http://localhost:8000/


# Tests
Pytest and coverage configuration is in `setup.cfg`. To run all project tests with coverage report:

```shell
pytest --cov .
```

or with make (will also run check): `make test`

# Code Linting
tools used to check code quality are `black`, `isort`, `mypy`, `flake8`

To run a full check use

```shell
isort --diff --check .; black --diff --check --color .; flake8 .; mypy .;
```

or with make: `make check`

To automatically format the project files using the above tools use:

```shell
isort .; black .;
```

or with make: `make format`


## Pre-commit hooks
Repo also contains `.pre-commit-config.yaml`. This can be installed on your local clone as 
pre-commit hook (https://pre-commit.com/) to run the various code-linting tools. 
This is optional, but strongly advised as it can prevent commits that break linting checks.

```shell
pre-commit install
```


# Development (useful tips)

* Quick dev setup: use `.env` file, set `DATABASE_ENGINE=sqlite3`, run migrate and launch devserver
(see "Run native devserver" above)

* Use dev fixtures to populate DB with some demo data. Run `make load-dev-fixtures`


# Authors
Evangelos Doukakis <evangelos.doukakis@gmail.com>, July 2022

# Contributing
N/A

# License
N/A. Demo code.

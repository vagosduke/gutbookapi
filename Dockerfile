FROM python:3.9-slim

ADD requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install gunicorn==20.1.0

ADD . .

CMD gunicorn gutbookapi.wsgi -b 0.0.0.0:8000
#CMD python manage.py runserver 0.0.0.0:8000

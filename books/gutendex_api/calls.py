import logging
from typing import List, Union, cast

import requests
from django.conf import settings
from django.core.cache import cache

logger = logging.getLogger("django.request")


def search_gutendex(term: str, page: int = None):
    """
    Perform the API call to gutendex.com/books?search=...
    """
    params = {"search": term}
    if page is not None:
        params["page"] = str(page)
    try:
        result = requests.get(settings.GUTENDEX_SEARCH_URL, params=params)
        result_json = result.json()
    except Exception as e:
        logger.error(f"An error occurred during search request for term '{term}': {e}")
        return None
    else:
        return result_json


def select_gutendex(book_ids: List[int]):
    """
    Perform the API call to gutendex.com/books?ids=...
    :param book_ids:
    :return:
    """
    ids = ",".join([str(s) for s in book_ids])
    try:
        result = requests.get(settings.GUTENDEX_SEARCH_URL, params={"ids": ids})
        result_json = result.json().get("results")
    except Exception as e:
        logger.error(f"An error occurred during fetch request for ids '{ids}': {e}")
        return None
    else:
        return result_json


def search_books(term: str, page: int = None, use_cache: bool = False) -> Union[dict, None]:
    """
    Make the call to gutendex API and return the results
    """
    if not term:
        logger.debug("Request to search null term")
        return None
    result_json = search_gutendex(term, page)
    # TODO: can also store results in cache
    return result_json


def get_books(book_id: Union[int, List[int]], use_cache: bool = False) -> Union[list, None]:
    if not book_id:
        logger.debug("Request to get null book_id")
        return None
    if type(book_id) is not list:
        book_id_list = [cast(int, book_id)]
    else:
        book_id_list = cast(List[int], book_id)

    if use_cache:
        cached_books = cache.get_many(book_id_list)
        non_cache_list = [x for x in book_id_list if cached_books.get(x) is None]
    else:
        cached_books = {}
        non_cache_list = book_id_list

    # Fetch non-cached books
    if non_cache_list:
        result_json = select_gutendex(non_cache_list)
    else:
        result_json = []

    # Store fetched books back in cache
    fetched_books = {}
    for value in result_json:
        fetched_books[value.get("id")] = value
    if use_cache and fetched_books:
        cache.set_many(fetched_books)

    # Return both cached and fetched results
    ret_list = list(cached_books.values()) + list(result_json)
    return ret_list

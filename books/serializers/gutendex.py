from rest_framework import serializers


class AuthorSerializer(serializers.Serializer):
    name = serializers.CharField()
    birth_year = serializers.IntegerField(required=False, allow_null=True)
    death_year = serializers.IntegerField(required=False, allow_null=True)


class BookSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    title = serializers.CharField()
    authors = AuthorSerializer(many=True, required=False, allow_empty=True)
    languages = serializers.ListSerializer[serializers.CharField](child=serializers.CharField(), required=False, allow_empty=True)
    download_count = serializers.IntegerField(required=False, allow_null=True)

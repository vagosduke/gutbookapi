from django.core.validators import MaxValueValidator, MinValueValidator
from rest_framework import serializers

from books.models import BookRating
from books.serializers.gutendex import BookSerializer


class BookRatingSerializer(serializers.ModelSerializer):
    bookId = serializers.IntegerField(source="book_id")
    rating = serializers.IntegerField(source="rating_value", validators=[MinValueValidator(0), MaxValueValidator(5)])
    review = serializers.CharField(source="review_text", allow_blank=True, required=False)

    class Meta:
        model = BookRating
        fields = ("bookId", "rating", "review")


class RatedBookSerializer(BookSerializer):
    rating = serializers.FloatField(validators=[MinValueValidator(0.0), MaxValueValidator(5.0)], allow_null=True)
    reviews = serializers.ListSerializer[serializers.CharField](
        child=serializers.CharField(), default=[], allow_null=True, allow_empty=True
    )

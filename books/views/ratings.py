import logging

from rest_framework.response import Response
from rest_framework.views import APIView

from books.gutendex_api.calls import get_books
from books.serializers.ratings import BookRatingSerializer

logger = logging.getLogger("django.request")


class RatingAPIView(APIView):
    def get_view_name(self):
        return "Rating System View"

    def post(self, request, format=None):
        # validate that the book exists
        book_id = request.data.get("bookId")
        result = get_books(book_id)
        if result is None:
            return Response({"success": "False", "details": f"An error occurred trying to find book id '{book_id}'"}, status=505)
        if len(result) == 0:
            return Response({"success": "False", "details": f"No book id '{book_id}'. Cannot submit rating"}, status=404)
        # assume that the book exists at this point
        deserialized_rating = BookRatingSerializer(data=request.data)
        if deserialized_rating.is_valid():
            # BookRating.objects.create(**deserialized_rating)
            rating = deserialized_rating.save()
            return Response({"success": "True", "id": rating.pk})
        else:
            return Response({"success": "False", "details": deserialized_rating.errors})

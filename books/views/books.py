import logging

from django.urls import reverse
from django.utils.http import urlencode
from rest_framework.response import Response
from rest_framework.views import APIView

from books.gutendex_api.calls import get_books, search_books
from books.models import BookRating
from books.serializers.gutendex import BookSerializer
from books.serializers.ratings import RatedBookSerializer

logger = logging.getLogger("django.request")


def _append_book_ratings(book_id: int, book_dict: dict) -> dict:
    book_ratings = BookRating.objects.filter(book_id=book_id)
    if not book_ratings:
        average_rating = None
        book_reviews = []
    else:
        book_rating_values = book_ratings.values_list("rating_value", flat=True)
        average_rating = sum(book_rating_values) / len(book_rating_values)
        book_reviews = list(book_ratings.values_list("review_text", flat=True))
        book_reviews = [review for review in book_reviews if review and review.strip()]
    book_dict.update({"rating": average_rating, "reviews": book_reviews})
    return book_dict


class SearchBooksAPIView(APIView):
    def get_view_name(self):
        return "Search Books"

    def get(self, request, format=None):
        term = request.GET.get("term", "")
        page = request.GET.get("page")
        if not term:
            return Response({"success": "False", "details": "No search term given"}, status=400)
        result = search_books(term, page)
        book_results = result.get("results")

        # use the same paging as Gutendex search API
        api_next_page = result.get("next")
        api_previous_page = result.get("previous")
        paging_info = {
            "next": None,
            "previous": None,
        }
        if page is None:
            page = 1
        else:
            page = int(page)
        url = reverse("search-books")
        if api_next_page:
            next_page = page + 1
            paging_info["next"] = f'{url}?{urlencode({"term": term, "page": next_page})}'
        if api_previous_page:
            if page > 1:
                prev_page = page - 1
                paging_info["previous"] = f'{url}?{urlencode({"term": term, "page": prev_page})}'
            else:
                paging_info["previous"] = f'{url}?{urlencode({"term": term})}'

        if book_results is None:
            return Response({"success": "False", "details": f"An error occurred during searching '{term}'"}, status=505)
        # serialize and serve results
        serialized_results = BookSerializer(data=book_results, many=True)
        if serialized_results.is_valid():
            response = {"success": "True", "books": serialized_results.data}
            response.update(paging_info)
            return Response(response)
        else:
            logger.error(f"Cannot parse results for '{term}': {serialized_results.errors}")
            return Response({"success": "False", "details": "Cannot parse results"}, status=505)


class BookDetailsAPIView(APIView):
    def get_view_name(self):
        return "Book Details"

    def get(self, request, book_id, format=None):
        # fetch book or used cached
        result = get_books(book_id)

        if result is None:
            return Response({"success": "False", "details": f"An error occurred fetching book id '{book_id}'"}, status=500)
        if len(result) == 0:
            return Response({"success": "False", "details": f"No book with id '{book_id}' found"}, status=404)
        result = result[0]  # if successful, call returns exactly one book
        # append ratings
        appended_result = _append_book_ratings(book_id, result)
        serialized_results = RatedBookSerializer(data=appended_result)
        if serialized_results.is_valid():
            return Response({"success": "True", "books": serialized_results.data})
        else:
            logger.error(f"Not valid results for book with id '{book_id}': {serialized_results.errors}")
            return Response({"success": "False", "details": f"Not valid results for book with id '{book_id}' "}, status=500)


class TopRatedBooksAPIView(APIView):
    def get_view_name(self):
        return "Top Rated Books"

    def get(self, request, format=None):
        top_books_qs = BookRating.top_ratings()
        book_ids = [x["book_id"] for x in list(top_books_qs)]

        # fetch book or used cached
        result = get_books(book_ids)
        if result is None:
            return Response({"success": "False", "details": "An error occurred fetching top books"}, status=500)
        if len(result) == 0:
            return Response({"success": "False", "details": "No top books found"}, status=404)

        for book_result in result:
            book_id = book_result["id"]
            # TODO: Techdebt: improve performance by making only one DB request for all books
            _append_book_ratings(book_id, book_result)  # append dicts in-place (mutable)

        result = sorted(result, key=lambda x: x["rating"], reverse=True)
        serialized_results = RatedBookSerializer(data=result, many=True)
        if serialized_results.is_valid():
            return Response({"success": "True", "books": serialized_results.data})
        else:
            logger.error(f"Not valid results for top books: {serialized_results.errors}")
            return Response({"success": "False", "details": "Not valid results for top books "}, status=500)

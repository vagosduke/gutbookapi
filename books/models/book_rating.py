from django.conf import settings
from django.db import models
from django.db.models import Avg, QuerySet


class BookRatingManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()


class BookRating(models.Model):
    book_id = models.IntegerField(help_text="Numeric rating of a product 0-5")
    rating_value = models.IntegerField(help_text="Numeric rating of a product 0-5")
    review_text = models.TextField(default="", blank=True, help_text="User review text")
    submitted_on = models.DateTimeField(auto_now_add=True)

    objects = BookRatingManager()

    class Meta:
        verbose_name = "Book Rating"
        verbose_name_plural = "Book Ratings"
        ordering = ("-submitted_on",)

    def __str__(self):
        return f"Rating [{self.rating_value}] for id:{self.book_id} ({self.submitted_on}) - {self.review_text}"

    @classmethod
    def top_ratings(cls, limit: int = settings.TOP_BOOKS_LIMIT) -> QuerySet:
        queryset = cls.objects.values("book_id").annotate(avg_rating=Avg("rating_value")).order_by("-avg_rating")[:limit]
        return queryset

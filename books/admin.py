from django.contrib import admin

from books.models import BookRating


class BookRatingAdmin(admin.ModelAdmin):

    model = BookRating
    list_display = ["book_id", "rating_value", "review_text", "submitted_on"]
    search_fields = ["book_id"]
    readonly_fields = ["book_id", "submitted_on"]
    ordering = ["-submitted_on"]


admin.site.register(BookRating, BookRatingAdmin)

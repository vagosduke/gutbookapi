from django.urls import path

from books.views.books import BookDetailsAPIView, SearchBooksAPIView, TopRatedBooksAPIView
from books.views.ratings import RatingAPIView

urlpatterns = [
    path("books/search", SearchBooksAPIView.as_view(), name="search-books"),
    path("books/<int:book_id>", BookDetailsAPIView.as_view(), name="book-details"),
    path("books/top", TopRatedBooksAPIView.as_view(), name="top-books"),
    path("books/rating", RatingAPIView.as_view(), name="book-rating"),
]

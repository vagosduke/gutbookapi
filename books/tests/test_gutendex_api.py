from books.gutendex_api.calls import search_books


def test_search_books():
    """
    Integration test.
    Validates that the API calls to gutendex.com work.
    """
    check_keys = ["id", "title", "authors", "languages"]
    result = search_books("hello", use_cache=False)
    assert "results" in result

    book_results = result["results"]
    assert type(book_results) is list
    for book in book_results:
        for key in check_keys:
            assert key in book


def test_get_books():
    pass

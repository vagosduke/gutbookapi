import pytest

from books.models import BookRating


@pytest.mark.django_db
def test_top_books(dev_db_setup):
    # WARNING! Asserted values below match the dev fixtures. If changing the dev fixtures, tests WILL fail
    top_books = BookRating.top_ratings(2)
    assert len(top_books) == 2
    assert top_books[0]["book_id"] == 401
    assert top_books[0]["avg_rating"] == 5
    assert top_books[1]["book_id"] == 201
    assert pytest.approx(top_books[1]["avg_rating"], 0.1) == 4.7

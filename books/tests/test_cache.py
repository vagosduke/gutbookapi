from django.conf import settings
from django.core.cache import cache
from django_fakeredis import FakeRedis

from books.gutendex_api.calls import get_books


@FakeRedis("django.core.cache.cache")
def test_cached_get_books(mocker):
    """
    Integration test.
    Validates that the API calls to gutendex.com work.
    """
    ret_list = [
        {
            "id": 100,
            "title": "The Complete Works of William Shakespeare",
            "authors": [{"name": "Shakespeare, William"}],
            "languages": ["en"],
        }
    ]

    def mock_select_gutendex(book_ids):
        return ret_list

    print(settings.CACHES)

    mocked_select = mocker.patch("books.gutendex_api.calls.select_gutendex", side_effect=mock_select_gutendex)

    cache.clear()
    assert cache.get(100) is None
    result = get_books([100, 101], use_cache=True)
    assert result == ret_list
    assert cache.get(100) == ret_list[0]
    assert mocked_select.call_count == 1

    result = get_books(100, use_cache=True)
    assert result == ret_list
    assert mocked_select.call_count == 1  # call count is still 1

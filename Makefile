.PHONY: check format test devserver dump-dev-fixtures load-dev-fixtures

check:
	isort --diff --check .
	black --diff --check --color .
	flake8 .
	mypy .

format:
	isort .
	black .

test:
	pytest --cov .

migrations:
	python manage.py makemigrations books

migrate:
	python manage.py migrate

load-dev-fixtures:
	python manage.py loaddata books/fixtures/data/dev_ratings.json

build:
	docker build -t gutbookapi .

devserver:
	python manage.py runserver 0.0.0.0:8000

start-db:
	docker-compose up -d postgres

start-cache:
	docker-compose up -d redis
